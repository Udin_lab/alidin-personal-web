import { faBars, faHamburger } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { Outlet } from "react-router-dom";
import Sidebar from "../components/static/Sidebar";

function MainLayout(props) {
	const [isOpened, setIsOpened] = useState();

	return (
		<div className='flex flex-row h-full '>
			<div
				className={
					"fixed top-0  flex-shrink-0 h-full p-4 overflow-y-auto w-72 bg-secondary-dark transition-all duration-200 " +
					(isOpened ? "-left-72" : "left-0")
				}
			>
				<Sidebar />
				<div
					className={
						"fixed top-2 transition-all duration-200 print:hidden " +
						(isOpened ? "left-0" : "left-72")
					}
				>
					<button
						onClick={() => setIsOpened(!isOpened)}
						className='btn-secondary bg-secondary-dark rounded-l-none w-10'
					>
						<FontAwesomeIcon icon={faBars} />
					</button>
				</div>
			</div>
			<div
				className={
					"flex flex-col flex-grow p-15 transition-all duration-200 " +
					(isOpened ? "ml-0" : "lg:ml-72 ml-0 print:ml-72")	
				}
			>
				<Outlet />
			</div>
		</div>
	);
}

export default MainLayout;
