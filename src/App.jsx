import React from "react";
import { HashRouter, Route, Routes } from "react-router-dom";
import MainLayout from "./layouts/MainLayout";
import PageNotFound from "./layouts/PageNotFound";
import AboutMe from "./pages/about-me/AboutMe";
import Contents from "./pages/content/Contents";
import Contact from "./pages/contact/Contact";
import Portofolio from "./pages/portofolio/Portofolio";
import ProjectDetail from "./pages/portofolio/[id]/ProjectDetail";
import Resume from "./pages/resume/Resume";
import ServicePricing from "./pages/service-pricing/ServicePricing";
import PortofolioLayout from "./pages/portofolio/PortofolioLayout";
import CurriculumVitae from "./pages/curriculum-vitae/CurriculumVitae";

function App(props) {
	return (
		<div className='h-full '>
			<HashRouter>
				<Routes>
					<Route path='/' element={<MainLayout />}>
						<Route index element={<AboutMe />}></Route>
						<Route path='portofolio' element={<PortofolioLayout />}>
							<Route index element={<Portofolio />}></Route>
							<Route path=':id' element={<ProjectDetail />}></Route>
						</Route>
						<Route path='services' element={<ServicePricing />}></Route>
						<Route path='cv' element={<CurriculumVitae />}></Route>
						<Route path='resume' element={<Resume />}></Route>
						<Route path='blog' element={<Contents />}></Route>
						<Route path='contact' element={<Contact />}></Route>
					</Route>
					<Route path='print-cv' element={<CurriculumVitae />}></Route>
					<Route path='/*' element={<PageNotFound />}></Route>
				</Routes>
			</HashRouter>
		</div>
	);
}

export default App;
