import { faQuoteLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

function TestimonialItem(props) {
	return (
		<div className='relative flex flex-col gap-5 p-5 px-10 overflow-hidden bg-secondary-semi-alt '>
			<div className='absolute rotate-45 w-18 h-18 -top-10 -left-10 bg-primary-teal'></div>
			<div className='absolute top-0 left-0 px-1 text-lg font-extrabold'>
				<FontAwesomeIcon icon={faQuoteLeft} />
			</div>
			<p className='text-sm text-secondary-light'>
				Simon is a brilliant software engineer! Lorem ipsum dolor sit amet,
				consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
				massa. Cum sociis natoque penatibus et magnis.
			</p>
			<div className='flex flex-row items-center gap-5 '>
				<div className='w-16 h-16 bg-white rounded-full'></div>
				<div className='flex flex-col gap-2 text-xs text-secondary-light'>
					<span>Samuel Reyes</span>
					<span>CEO LPP NTB</span>
				</div>
			</div>
		</div>
	);
}

export default TestimonialItem;
