import React from 'react';
import { Link } from 'react-router-dom';

function ContentItem(props) {
	return (
		<div className='flex flex-col bg-secondary-semi-alt '>
			<div className='bg-white aspect-square '></div>
			<div className='flex flex-col gap-2 p-5 '>
				<Link
					to='#'
					className='underline text-primary-light hover:text-primary-teal'
				>
					<h5>Kenapa Udin Suka JavaScript</h5>
				</Link>
				<div className=''>
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
					commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus
					et magnis dis parturient...
				</div>
				<Link to='#' className='text-primary-teal'>
					Read More...
				</Link>
				<div className='text-sm text-secondary-light'>Published 2 days ago</div>
			</div>
		</div>
	);
}

export default ContentItem;
