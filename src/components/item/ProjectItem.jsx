import React from "react";

import PropTypes from "prop-types";
import propTypes from "prop-types";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCode, faLink } from "@fortawesome/free-solid-svg-icons";

ProjectItem.propTypes = {
	project: PropTypes.shape({
		id: PropTypes.string,
		title: PropTypes.string,
		icon: PropTypes.object,
		tag: PropTypes.arrayOf(PropTypes.string),
		client: PropTypes.string,
		tech: PropTypes.array,
		urls: PropTypes.array,
		description: PropTypes.string,
		responsibily: PropTypes.arrayOf(PropTypes.string),
		challenge: PropTypes.string,
		result: propTypes.arrayOf(PropTypes.object),
		images: PropTypes.arrayOf(PropTypes.string),
	}),
};

function ProjectItem(props) {
	const { project } = props;
	return (
		<div className='flex flex-row bg-secondary-semi-alt'>
			<div className='flex-shrink-0 bg-primary-light h-52 w-52 '>
				<div className='h-full w-full'>
					<img src={project.icon} />
				</div>
			</div>
			<div className='flex flex-col gap-2 px-8 py-4  justify-between'>
				<div className='flex flex-col gap-2'>
					<Link to={'/portofolio/'+project.id}>
						<h5 className='underline text-base cursor-pointer select-none hover:text-primary-teal'>
							{project.title}
						</h5>
					</Link>
					<p className='text-secondary-light text-sm '>
						{project.description.substring(0, 100) + "..."}
					</p>
					<span className='text-sm text-secondary-light inline-flex gap-2'>
						<FontAwesomeIcon icon={faCode} />
						{project.tech.join(", ")}
					</span>
				</div>
				{project.description.length > 100 && (
					<Link
						className='text-primary-light text-sm hover:text-primary-teal'
						to={'/portofolio/'+project.id}
					>
						Read More <FontAwesomeIcon icon={faLink} />
					</Link>
				)}
			</div>
		</div>
	);
}

export default ProjectItem;
