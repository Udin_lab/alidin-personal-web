import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import propTypes from "prop-types";

SkillItem.propTypes = {
	skill: PropTypes.shape({
		icons: PropTypes.arrayOf(PropTypes.any).isRequired,
		title: PropTypes.string.isRequired,
		description: PropTypes.arrayOf(propTypes.string).isRequired,
	}),
};

SkillItem.defaultProps = {
	skill: {
		icons: [],
		title: "",
		description: [],
	},
};

function SkillItem(props) {
	const { skill } = props;
	return (
		<div className='flex flex-col'>
			<div className='flex flex-row gap-2'>
				{skill.icons.map((icon, index) => (
					<div className='h-10'>
						<FontAwesomeIcon className='w-8 h-8' icon={icon} key={index} />
					</div>
				))}
			</div>
			<span className='font-extrabold underline'>{skill.title}</span>
			{skill.description.map((description, index) => (
				<p className='text-sm indent-5 ' key={index}>
					{description}
				</p>
			))}
		</div>
	);
}

export default SkillItem;
