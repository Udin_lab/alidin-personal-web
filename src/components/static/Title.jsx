import React from 'react';
import PropTypes from 'prop-types';

Title.propTypes = {
	children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
};

function Title(props) {
	const { children } = props;
	return (
		<div className='px-5 py-2 border-l-8 border-l-primary-teal '>
			<h3>{children}</h3>
		</div>
	);
}

export default Title;
