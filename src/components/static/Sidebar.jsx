import {
	faGitlab,
	faInstagram,
	faLinkedin,
	faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import {
	faBlog,
	faBriefcase,
	faContactCard,
	faFile,
	faFolderClosed,
	faLaptopCode,
	faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { NavLink } from "react-router-dom";
import profilePp from "../../assets/images/alidin-informal.jpg";

function Sidebar(props) {

	return (
		<div className='flex flex-col items-center gap-4 print:pt-15'>
			<h4>Alidin</h4>
			<div className='w-40 h-40 bg-white border rounded-full overflow-hidden'>
				<img className='w-full h-full' src={profilePp}></img>
			</div>
			<div className='text-sm text-center print:hidden'>
				Hi, my name is Alidin and I'm a junior fullstack engineer. Welcome to my
				personal website!
			</div>
			<div className='flex flex-row w-full mb-1 justify-evenly print:hidden'>
				<a
					className='sidebar-contact-icon'
					href='https://www.instagram.com/udin.jsx/?hl=id'
					target={"_blank"}
				>
					<FontAwesomeIcon icon={faInstagram} />
				</a>
				<a
					className='sidebar-contact-icon'
					href='https://gitlab.com/Udin_lab'
					target={"_blank"}
				>
					<FontAwesomeIcon icon={faGitlab} />
				</a>
				<a
					className='sidebar-contact-icon'
					href='https://www.linkedin.com/in/alidin-alidin-33808223a/'
					target={"_blank"}
				>
					<FontAwesomeIcon icon={faLinkedin} />
				</a>
				<a
					className='sidebar-contact-icon'
					href='https://www.youtube.com/channel/UC5X9HqFvk1BTjNFtvwMiAOA'
					target={'_blank'}
				>
					<FontAwesomeIcon icon={faYoutube} />
				</a>
			</div>
			<hr className='w-full print:border-t-8 border-primary-teal ' />
			<div className='flex flex-col gap-2 w-44 print:hidden'>
				<NavLink
					to='/'
					className={(navData) =>
						"navbar-item " +
						(navData.isActive && "text-primary-teal hover:text-secondary-teal ")
					}
				>
					<div className='w-6 text-center'>
						<FontAwesomeIcon icon={faUser} />
					</div>
					About Me
				</NavLink>
				<NavLink
					to='cv'
					className={(navData) =>
						"navbar-item " +
						(navData.isActive && "text-primary-teal hover:text-secondary-teal ")
					}
				>
					<div className='w-6 text-center'>
						<FontAwesomeIcon icon={faBriefcase} />
					</div>
					Curriculum Vitae
				</NavLink>
				<NavLink
					to='portofolio'
					className={(navData) =>
						"navbar-item " +
						(navData.isActive && "text-primary-teal hover:text-secondary-teal ")
					}
				>
					<div className='w-6 text-center'>
						<FontAwesomeIcon icon={faLaptopCode} />
					</div>
					Portofolio
				</NavLink>

				{/* <NavLink
					to='resume'
					className={(navData) =>
						"navbar-item " +
						(navData.isActive && "text-primary-teal hover:text-secondary-teal ")
					}
				>
					<div className='w-6 text-center'>
						<FontAwesomeIcon icon={faFile} />
					</div>
					Resume
				</NavLink> */}
				{/* <NavLink
					to='blog'
					className={(navData) =>
						"navbar-item " + (navData.isActive && "text-primary-teal")
					}
				>
					<div className='w-6 text-center'>
						<FontAwesomeIcon icon={faFolderClosed} />
					</div>
					Contents
				</NavLink> */}
				{/* <NavLink
					to='contact'
					className={(navData) =>
						"navbar-item " + (navData.isActive && "text-primary-teal")
					}
				>
					<div className='w-6 text-center'>
						<FontAwesomeIcon icon={faContactCard} />
					</div>
					Contact
				</NavLink> */}
			</div>
			<div className='hidden fixed top-400 font-bold text-8xl -rotate-90 left-20 origin-top-left print:flex print:text-gray-300 '>
				PORTOFOLIO
			</div>
		</div>
	);
}

export default Sidebar;
