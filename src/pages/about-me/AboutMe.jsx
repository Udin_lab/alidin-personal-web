import {
	faBriefcase,
	faFile,
	faLaptopCode,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import LatestContents from "./component/LatestContent";
import FeaturedProjects from "./component/FeaturedProjects";
import SkillList from "./component/SkillList";
import Testimonials from "./component/Testimonials";
import SertificationGallery from "./component/SertificationGallery";
import { Link } from "react-router-dom";

function AboutMe(props) {
	return (
		<div className='flex flex-col gap-15 pb-15 '>
			<div className='flex flex-col lg:w-1/2 w-full gap-5 '>
				<div>
					<h1>Alidin</h1>
					<span className='text-xl '>Junior fullstack developer</span>
				</div>
				<p className=''>
					I'm a software engineer specialised in fullstack development from
					small to complex scalable web apps. Want to know how I may help your
					project? Check out my project portfolio and online CV.
				</p>
				<div className='flex flex-row gap-2'>
					<Link to='/portofolio'>
						<button className='w-44 btn-primary'>
							<span className='space-x-2'>
								<FontAwesomeIcon icon={faLaptopCode} />
								<span>View Portofolio</span>
							</span>
						</button>
					</Link>
					<Link to='/cv'>
						<button className='w-44 btn-secondary'>
							<span className='space-x-2'>
								<FontAwesomeIcon icon={faFile} />
								<span>View CV</span>
							</span>
						</button>
					</Link>
				</div>
			</div>
			<hr />
			<SkillList />
			<hr />
			<FeaturedProjects />
			{/* <hr />
			<SertificationGallery /> */}
		</div>
	);
}

export default AboutMe;
