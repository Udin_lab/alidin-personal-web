import React from "react";
import PropTypes from "prop-types";
import SertificationItem from "./SertificationItem";
import Title from "../../../components/static/Title";

SertificationGallery.propTypes = {};

function SertificationGallery(props) {
	return (
		<div className='flex flex-col gap-5 '>
			<Title>Gallery of Sertificates</Title>
			<div className='grid grid-cols-2 gap-5'>
        <SertificationItem/>
			</div>
		</div>
	);
}

export default SertificationGallery;
