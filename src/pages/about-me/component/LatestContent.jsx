import { faFolderClosed } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import ContentItem from '../../../components/item/ContentItem';
import Title from '../../../components/static/Title';

function LatestContents(props) {
	return (
		<div className='flex flex-col gap-5'>
			<Title>Latest Contents</Title>
			<div className='grid grid-cols-3 gap-5'>
				<ContentItem />
				<ContentItem />
				<ContentItem />
			</div>
			<div className='inline-flex justify-center'>
				<button className='w-44 btn-primary'>
					<span className='space-x-2'>
						<FontAwesomeIcon icon={faFolderClosed} />
						<span>View My Contents</span>
					</span>
				</button>
			</div>
		</div>
	);
}

export default LatestContents;
