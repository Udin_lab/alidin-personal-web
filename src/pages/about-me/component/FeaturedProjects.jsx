import { faFile } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import ProjectItem from "../../../components/item/ProjectItem";
import Title from "../../../components/static/Title";
import featuredProjectData from "../../../assets/data/featured-project-data";
import portofolioData from "../../../assets/data/portofolio-data";

function FeaturedProjects(props) {
	return (
		<div className='flex flex-col gap-5 '>
			<Title>Featured Projects</Title>
			<div className='grid lg:grid-cols-2 grid-cols-1 gap-5 '>
				{portofolioData.map((item) => (
					<ProjectItem project={item} key={item.id} />
				))}
			</div>

			<div className='inline-flex justify-center'>
				<button className='w-44 btn-primary'>
					<span className='space-x-2'>
						<FontAwesomeIcon icon={faFile} />
						<span>View Portofolio</span>
					</span>
				</button>
			</div>
		</div>
	);
}

export default FeaturedProjects;
