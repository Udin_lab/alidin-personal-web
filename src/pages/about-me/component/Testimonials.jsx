import React from 'react';
import TestimonialItem from '../../../components/item/TestimonialItem';
import Title from '../../../components/static/Title';

function Testimonials(props) {
	return (
		<div className='flex flex-col gap-5'>
			<Title>Testimonials</Title>
			<div className='grid grid-cols-3 gap-5 '>
				<TestimonialItem />
				<TestimonialItem />
				<TestimonialItem />
				<TestimonialItem />
				<TestimonialItem />
				<TestimonialItem />
			</div>
		</div>
	);
}

export default Testimonials;
