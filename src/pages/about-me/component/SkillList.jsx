import {
	faJava,
	faJs,
	faPhp,
	faPython,
} from "@fortawesome/free-brands-svg-icons";
import { faBriefcase } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import SkillItem from "../../../components/item/SkillItem";
import Title from "../../../components/static/Title";
import icandoData from "../../../assets/data/icando-data";

function SkillList(props) {
	return (
		<div className='flex flex-col gap-5 '>
			<Title>What can i do</Title>
			<p>
				I have 1.5 years' experience building software for clients and at the same
				time I still focused on my study as a Informatics Engineering student at
				University of Mataram. Below is a quick overview of my main technical
				skill sets and technologies I use. Want to find out more about my
				experience? Check out my online CV and project portfolio.
			</p>
			{/* skill list */}
			<div className='grid lg:grid-cols-4 md:grid-cols-2 grid-cols-1 gap-5'>
				{icandoData.map((skill, index) => (
					<SkillItem key={skill.id} skill={skill} />
				))}
			</div>
		</div>
	);
}

export default SkillList;
