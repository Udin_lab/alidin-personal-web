import {
	faBuildingColumns,
	faEnvelope,
	faFile,
	faLocationDot,
	faLocationPin,
	faPhone,
	faPrint,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import OtherProjects from "./component/OtherProjects";
import FeaturedProjects from "./component/FeaturedProjects";
import CvSidebar from "./component/CvSidebar";
import WorkExperience from "./component/WorkExperience";
import Biodata from "./component/Biodata";
import Sertificates from "./component/Sertificates";
import { faGitlab, faLinkedin } from "@fortawesome/free-brands-svg-icons";
import profilePicture from "../../assets/images/alidin.png";
import { Link } from "react-router-dom";

function CurriculumVitae(props) {
	return (
		<div className='flex flex-col p-10 bg-secondary-dark gap-5 print:text-primary-dark print:p-30 min-w-200 lg:min-w-full'>
			<div className='flex flex-row justify-between'>
				<div className='flex flex-col border-b-8 border-primary-teal'>
					<h1>Alidin</h1>
				</div>
				<div className='flex flex-col'>
					<span className='inline-flex items-center gap-2'>
						<div className=' text-primary-dark bg-gray-200 rounded w-5 h-5 grid'>
							<FontAwesomeIcon icon={faPhone} className='w-3 m-auto' />
						</div>
						<span>081337884868</span>
					</span>
					<span className='inline-flex items-center gap-2'>
						<div className=' text-primary-dark bg-gray-200 rounded w-5 h-5 grid'>
							<FontAwesomeIcon icon={faEnvelope} className='w-3 m-auto' />
						</div>
						<span>Alidin202@gmail.com</span>
					</span>
					<span className='inline-flex items-center gap-2'>
						<div className=' text-primary-dark bg-gray-200 rounded w-5 h-5 grid'>
							<FontAwesomeIcon icon={faGitlab} className='w-3 m-auto' />
						</div>
						<span>gitlab.com/Udin_lab</span>
					</span>
				</div>
			</div>
			<div className='flex flex-row gap-5 items-center'>
				<div>
					<div className='rounded-full bg-white h-32 w-32 overflow-hidden'>
						<img className='w-full' src={profilePicture}></img>
					</div>
				</div>
				<div>
					I'm a junior full stack developer with about 1.5 years of experience,
					I'm a person who wants to master everything related to something I
					like, especially if it's related to programming, critical thinking,
					and problem solving. I don't know when to give up when faced with
					complex programming cases that require critical thinking and accurate
					problem solving. for this reason, I decided to pursue a career in the
					field of fullstack website development and sometimes I'm also working
					on Machine Learning and Artificial Intelligence Project.
				</div>
			</div>

			<hr className='border-gray-500 ' />

			<div className='flex flex-row'>
				<div className='flex flex-col gap-3 border-r border-gray-500 pr-7'>
					{/* <Biodata /> */}
					<WorkExperience />
					<FeaturedProjects />
					<Sertificates />
				</div>
				<div className='min-w-72 flex flex-col pl-7'>
					<CvSidebar />
				</div>
			</div>

			<div className='inline-flex items-center justify-center print:hidden border-t border-gray-500'>
				<a href='https://www.linkedin.com/in/alidin-alidin-33808223a/'>
					<span className='space-x-2 '>
						<FontAwesomeIcon icon={faLinkedin} />
						<span>linkedin.com/in/alidin-alidin-33808223a</span>
					</span>
				</a>
			</div>

			<div className='inline-flex items-center justify-center print:hidden'>
				<Link to='/print-cv'>
					<button className='w-44 btn-secondary'>
						<span className='space-x-2'>
							<FontAwesomeIcon icon={faPrint} />
							<span>Print CV</span>
						</span>
					</button>
				</Link>
			</div>
		</div>
	);
}

export default CurriculumVitae;
