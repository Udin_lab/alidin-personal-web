import React from "react";
import PropTypes from "prop-types";

FeaturedProjects.propTypes = {};

function FeaturedProjects(props) {
	return (
		<div className='flex flex-col gap-3'>
			<span className='border-l-[7px] border-primary-teal pl-3'>
				<h5>FEATURED PROJECTS</h5>
			</span>
			<div className='flex flex-col gap-2 ml-5'>
				{/* items goes here */}
				<div className='flex flex-col '>
					<div className='flex flex-row justify-between'>
						<span className='font-bold'>
							TOEFL-UP : TOEFL Test Prediction App
						</span>
						<span className='text-gray-400 print:text-gray-600 text-sm'>
							Fullstack Developer
						</span>
					</div>
					<p className='text-sm'>
						This application is a part of project for campus intership, made
						specifically to predict TOEFL scores of participants that enrolled
						into Rumah Bahasa program LPPNTB, this application has been
						developed up to version 3.0, and is still actively used to conduct
						regular TOEFL exams. Developed by using ReactJs and Laravel.
					</p>
				</div>
				<div className='flex flex-col '>
					<div className='flex flex-row justify-between'>
						<span className='font-bold'>
							Segara Katon E-Sign and E-Form System
						</span>
						<span className='text-gray-400 print:text-gray-600 text-sm'>
							Fullstack Developer
						</span>
					</div>
					<p className='text-sm'>
						This application manage data collection of incoming and outgoing
						letters, as well as online signature service for Segara village's
						office, the frontend of this application are developed using ReactJs
						while the backend are developend using Laravel Rest API. I developed
						this application during my comunity service course 2022.
					</p>
				</div>
				<div className='flex flex-col '>
					<div className='flex flex-row justify-between'>
						<span className='font-bold'>
							Covidia : Covid-19 Detection System
						</span>
						<span className='text-gray-400 print:text-gray-600 text-sm'>
							Backend and Machine Learning Developer
						</span>
					</div>
					<p className='text-sm'>
						Successfully developed a new machine learning model that can
						accurately classify Covid-19, normal, and pneumonia lungs based on
						X-ray images with a 98% accuracy rate. The model is currently hosted
						using a Flask Rest API so another developer can access it to
						classify a large number of lung X-ray images. This project is led by
						I Gede Pasek Suta Wijaya, the Head of Informatics Engineering
						Department, Mataram University.
					</p>
				</div>
			</div>
		</div>
	);
}

export default FeaturedProjects;
