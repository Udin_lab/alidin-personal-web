import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faPhone } from "@fortawesome/free-solid-svg-icons";
import { faFacebook, faGitlab, faInstagram } from "@fortawesome/free-brands-svg-icons";

Biodata.propTypes = {};

function Biodata(props) {
	return (
		<div className='flex flex-col gap-3'>
			<span className='border-l-[7px] border-primary-teal pl-3'>
				<h5>BIODATA</h5>
			</span>
			<div className='pl-6 flex flex-col'>
				{/* items goes here */}
				<div className='flex flex-col gap-2'>
					<div className='inline-flex '>
						<span className='w-44'>Nama</span>
						<span>: Alidin</span>
					</div>
					<div className='inline-flex '>
						<span className='w-44'>TTL</span>
						<span>: Bima, 2 Februari 2000</span>
					</div>
					<div className='inline-flex '>
						<span className='w-44'>Jenis Kelamin</span>
						<span>: Laki-laki</span>
					</div>
					<div className='inline-flex '>
						<span className='w-44'>Alamat</span>
						<div className='inline-flex items-start gap-1'>
							<div>:</div>
							<span className='flex flex-col'>
								<span>Diponegoro Street No.34,</span>
								<span>Griya Praja Asri, Sesela, </span>
								<span>Lombok Barat</span>
							</span>
						</div>
					</div>
					<div className='inline-flex '>
						<span className='w-44'>Kontak</span>
						<div className='inline-flex items-start gap-1'>
							<div>:</div>
							<span className='flex flex-col'>
								<span className='inline-flex items-center gap-2'>
									<div className=' text-black bg-gray-200 rounded w-5 h-5 grid'>
										<FontAwesomeIcon icon={faPhone} className='w-3 m-auto' />
									</div>
									<span>081337884868</span>
								</span>
								<span className='inline-flex items-center gap-2'>
									<div className=' text-black bg-gray-200 rounded w-5 h-5 grid'>
										<FontAwesomeIcon icon={faEnvelope} className='w-3 m-auto' />
									</div>
									<span>Alidin202@gmail.com</span>
								</span>
                <span className='inline-flex items-center gap-2'>
									<div className=' text-black bg-gray-200 rounded w-5 h-5 grid'>
										<FontAwesomeIcon icon={faGitlab} className='w-3 m-auto' />
									</div>
									<span>@Udin_lab</span>
								</span>
                <span className='inline-flex items-center gap-2'>
									<div className=' text-black bg-gray-200 rounded w-5 h-5 grid'>
										<FontAwesomeIcon icon={faInstagram} className='w-3 m-auto' />
									</div>
									<span>@lord_udin</span>
								</span>
                <span className='inline-flex items-center gap-2'>
									<div className=' text-black bg-gray-200 rounded w-5 h-5 grid'>
										<FontAwesomeIcon icon={faFacebook} className='w-3 m-auto' />
									</div>
									<span>Alidin bin Abdul Malik</span>
								</span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Biodata;
