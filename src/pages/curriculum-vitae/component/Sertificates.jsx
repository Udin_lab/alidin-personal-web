import React from "react";
import PropTypes from "prop-types";

Sertificates.propTypes = {};

function Sertificates(props) {
	return (
		<div className='flex flex-col gap-3'>
			<span className='border-l-[7px] border-primary-teal pl-3'>
				<h5>SERTIFICATES</h5>
			</span>
			<div>
				<ul className='list-disc ml-10'>
					<li>
						Sertificate of Copyright : COVIDIA – COVID DIAGNOSE APPLICATION |
						KEMENTERIAN HUKUM DAN HAK ASASI MANUSIA REPUBLIK INDONESIA | 2021
					</li>
					<li>The Complete Full-Stack JavaScript Course | UDEMY | 2021 </li>
					<li>
						Belajar Membuat Aplikasi Back-End untuk Pemula | DICODING | 2021
					</li>
				</ul>
			</div>
		</div>
	);
}

export default Sertificates;
