import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faAngleDoubleRight,
	faAngleRight,
	faDotCircle,
	faLocationDot,
} from "@fortawesome/free-solid-svg-icons";

WorkExperience.propTypes = {};

function WorkExperience({ children }) {
	return (
		<div className='flex flex-col gap-3'>
			<span className='border-l-[7px] border-primary-teal pl-3'>
				<h5>WORK EXPERIENCES</h5>
			</span>
			<div className='pl-6 flex flex-col gap-2'>
				{/* items goes here */}

				<div className='flex flex-col gap-1'>
					<span className='flex flex-row justify-between'>
						<span className='font-bold'>
							Fullstack Developer
						</span>
						<spam className='text-sm text-gray-400 print:text-gray-600 inline-flex gap-2 items-center'>
							<FontAwesomeIcon icon={faLocationDot} />
							<span>Campus intership at LPPNTB | 2021</span>
						</spam>
					</span>
					<p className='text-sm'>
						Successfully created a 'TOEFL-UP : TOEFL Test Prediction App' by
						using React, and Laravel Rest API. up until now the system it self
						is currently at version 3 and still used to predict the TOEFL score
						of the student. Other than that, my main responsibility is to make
						sure the main website 'lppntb.com' is stable, secure and always up
						to date.
					</p>
				</div>

				<div className='flex flex-col gap-1'>
					<span className='flex flex-row justify-between'>
						<span className='font-bold'>Tech Officer</span>
						<spam className='text-sm text-gray-400 print:text-gray-600 inline-flex gap-2 items-center'>
							<FontAwesomeIcon icon={faLocationDot} />
							<span>Spacemed | 2022</span>
						</spam>
					</span>
					<p className='text-sm'>
						Spacemed is a startup under LPPNTB. My responsibility is to Create
						and develope the backend system for this startup called 'Space APP'
						such as Creating Database, Rest API, Maintaining the endpoint
						security, and making sure the system is stable 24/7 so the other
						developer can access the EndPoint at anytime. 'Space App' it self is
						an e-commerce app that provide place for people to buy and sell good
						and service that specialized for digital product such as Design,
						Website, Mobile APP, etc.
					</p>
				</div>

				<div className='flex flex-col gap-1'>
					<span className='flex flex-row justify-between'>
						<span className='font-bold'>Teaching Asistant</span>
						<spam className='text-sm text-gray-400 print:text-gray-600 inline-flex gap-2 items-center'>
							<FontAwesomeIcon icon={faLocationDot} />
							<span>Informatics Engineering Dept. - UNRAM | 2019-2021</span>
						</spam>
					</span>
					<p className='text-sm'>
						Successfully guided students to learn the basics concept of
						programming for each course, implement the algorithm into code, and
						make sure the students are able to work as team or alone so at the
						end of semester they can achieve an A grade for the course. As the
						teaching asistant, I also have to provide the students with the best
						possible learning path by using special modules that I provided by
						my self based on the tech trend and our current curriculum.
						<ul className='list-disc pl-5'>
							<li>Digital System | 2019 - 2020</li>
							<li>Basic Algorithm and Programming | 2019 - 2020</li>
							<li>Algorithm And Data Structure | 2019 - 2021</li>
							<li>Digital Image Processing | 2021</li>
						</ul>
					</p>
				</div>
			</div>
		</div>
	);
}

export default WorkExperience;
