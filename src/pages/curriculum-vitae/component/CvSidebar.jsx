import React from "react";
import PropTypes from "prop-types";

CvSidebar.propTypes = {};

function CvSidebar(props) {
	return (
		<div className='flex flex-col gap-3'>
			<div className='flex flex-col gap-3'>
				<span className='border-l-[7px] border-primary-teal pl-3'>
					<h5>EDUCATION</h5>
				</span>
				<div className='pl-10'>
					<ul className='list-disc'>
						<li>University of Mataram </li>
						<span className='flex flex-col text-sm text-secondary-light'>
							<span>Informatics Engineering</span>
							<span>2018-Now</span>
						</span>
						<li>SMA Negeri 1 Bolo</li>
						<span className='flex flex-col text-sm text-secondary-light'>
							<span>Science And Math</span>
							<span>2015-2018</span>
						</span>
					</ul>
				</div>
			</div>
			<div className='flex flex-col'>
				<span className='border-l-[7px] border-primary-teal pl-3'>
					<h5>SKILLS</h5>
				</span>
				<span className='font-bold pl-5'>Technical</span>

				<div className='pl-10 text-sm'>
					<ul className='list-disc'>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>JavaScript • React</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>Tailwind CSS • Bootsrap</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>MySQL • MongoDB</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>PHP • Laravel</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>Python • Tensorflow</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>Dart • Flutter</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Intermediate
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>Node.js • ExpressJs</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Intermediate
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>GO</span>{" "}
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Intermediate
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>GIT Version Control</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>Object-oriented design</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>Design and implement database structures</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between items-end'>
								<span>Lead and deliver complex software systems</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Advanced
								</span>
							</span>
						</li>
					</ul>
				</div>
				<span className='font-bold pl-5'>Professional</span>
				<div className='pl-10 text-sm'>
					<ul className='list-disc'>
						<li>Effective communication </li>
						<li>Team player </li>
						<li>Strong problem solver </li>
						<li>Good time management </li>
					</ul>
				</div>
			</div>

			<div className='flex flex-col gap-3'>
				<span className='border-l-[7px] border-primary-teal pl-3'>
					<h5>LANGUAGES</h5>
				</span>
				<div className='pl-10'>
					<ul className='list-disc'>
						<li>
							<span className='flex flex-row justify-between'>
								<span>Indonesia</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Native
								</span>
							</span>
						</li>
						<li>
							<span className='flex flex-row justify-between'>
								<span>English</span>
								<span className='text-gray-400 print:text-gray-600 text-right'>
									Intermediate
								</span>
							</span>
						</li>
					</ul>
				</div>
			</div>
			<div className='flex flex-col gap-3'>
				<span className='border-l-[7px] border-primary-teal pl-3'>
					<h5>INTEREST</h5>
				</span>
				<div className='pl-10'>
					<ul className='list-disc'>
						<li>Rubics Cube</li>
						<li>Puzzle</li>
						<li>Video Games</li>
					</ul>
				</div>
			</div>
		</div>
	);
}

export default CvSidebar;
