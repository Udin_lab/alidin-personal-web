import React from "react";
import PropTypes from "prop-types";

OtherProjects.propTypes = {};

function OtherProjects(props) {
	return (
		<div className='flex flex-col gap-3'>
			<span className='border-l-[7px] border-primary-teal pl-3'>
				<h5>OTHER PROJECT</h5>
			</span>
		</div>
	);
}

export default OtherProjects;
