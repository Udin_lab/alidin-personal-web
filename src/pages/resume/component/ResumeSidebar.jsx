import React from "react";
import PropTypes from "prop-types";

ResumeSidebar.propTypes = {};

function ResumeSidebar(props) {
	return (
		<div className='flex flex-col gap-3'>
			<div className='flex flex-col'>
				<span className='border-l-[7px] border-primary-teal pl-3'>
					<h5>SKILLS</h5>
				</span>
				<span className='font-bold pl-5'>Technical</span>

				<div className='pl-10 text-sm'>
					<ul className='list-disc'>
						<li>JavaScript/React</li>
						<li>Dart/Flutter</li>
						<li>Python/PHP/GO</li>
						<li>Node.js/ExpressJs</li>
						<li>MySQL/MongoDB</li>
						<li>Tailwind CSS/Bootsrap</li>
						<li>Object-oriented design</li>
						<li>Design and implement database structures</li>
						<li>Lead and deliver complex software systems</li>
					</ul>
				</div>
				<span className='font-bold pl-5'>Professional</span>
				<div className='pl-10 text-sm'>
					<ul className='list-disc'>
						<li>Effective communication </li>
						<li>Team player </li>
						<li>Strong problem solver </li>
						<li>Good time management </li>
					</ul>
				</div>
			</div>
			<div className='flex flex-col gap-3'>
				<span className='border-l-[7px] border-primary-teal pl-3'>
					<h5>EDUCATION</h5>
				</span>
				<div className='pl-10'>
					<ul className='list-disc'>
						<li>SMA Negeri 1 Bolo</li>
						<span className='flex flex-col text-sm text-secondary-light'>
							<span>Science Major</span>
							<span>2015-2018</span>
						</span>
						<li>University of Mataram </li>
						<span className='flex flex-col text-sm text-secondary-light'>
							<span>Informatics Engineering</span>
							<span>2018-Now</span>
						</span>
					</ul>
				</div>
			</div>
			<div className='flex flex-col gap-3'>
				<span className='border-l-[7px] border-primary-teal pl-3'>
					<h5>LANGUAGES</h5>
				</span>
				<div className='pl-10'>
					<ul className='list-disc'>
						<li>Indonesia (Native)</li>
						<li>English</li>
					</ul>
				</div>
			</div>
			<div className='flex flex-col gap-3'>
				<span className='border-l-[7px] border-primary-teal pl-3'>
					<h5>INTEREST</h5>
				</span>
				<div className='pl-10'>
					<ul className='list-disc'>
						<li>Rubics Cube</li>
						<li>Puzzle</li>
						<li>Video Games</li>
						<li>Comic</li>
						<li>Anime</li>
					</ul>
				</div>
			</div>
		</div>
	);
}

export default ResumeSidebar;
