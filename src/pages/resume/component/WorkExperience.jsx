import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationPin } from "@fortawesome/free-solid-svg-icons";

WorkExperience.propTypes = {};

function WorkExperience(props) {
	return (
		<div className='flex flex-col gap-3'>
			<span className='border-l-[7px] border-primary-teal pl-3'>
				<h5>WORK EXPERIENCES</h5>
			</span>
			<div className='pl-6 flex flex-col'>
				{/* items goes here */}
				<div className='flex flex-col gap-2'>
					<span className='flex flex-row justify-between'>
						<span className='font-bold'>Tech Officer</span>
						<spam className='text-sm text-gray-400 inline-flex gap-2 items-center'>
							<FontAwesomeIcon icon={faLocationPin} />
							<span>Spacemed | 2022</span>
						</spam>
					</span>
					<p className='text-sm'>
						Role description goes here ipsum dolor sit amet, consectetuer
						adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum
						sociis natoque penatibus et magnis dis parturient montes, nascetur
						ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu,
						pretium quis, sem. Donec pede justo, fringilla vel. Lorem ipsum
						dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula
						eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis
						parturient montes, nascetur ridiculus mus. Donec quam felis.
					</p>
				</div>
			</div>
		</div>
	);
}

export default WorkExperience;
