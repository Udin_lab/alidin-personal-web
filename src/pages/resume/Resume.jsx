import {
	faBuildingColumns,
	faEnvelope,
	faLocationDot,
	faLocationPin,
	faPhone,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import OtherProjects from "./component/OtherProjects";
import Projects from "./component/Projects";
import ResumeSidebar from "./component/ResumeSidebar";
import WorkExperience from "./component/WorkExperience";

function Resume(props) {
	return (
		<div className='flex flex-col p-10 bg-secondary-dark gap-5'>
			<div className='flex flex-row justify-between'>
				<div className='flex flex-col'>
					<h1>Alidin</h1>
					<span>Junior fullstack developer</span>
				</div>
				<div className='flex flex-col'>
					<span className='inline-flex items-center gap-2'>
						<div className=' text-black bg-gray-200 rounded w-5 h-5 grid'>
							<FontAwesomeIcon icon={faPhone} className='w-3 m-auto' />
						</div>
						<span>081337884868</span>
					</span>
					<span className='inline-flex items-center gap-2'>
						<div className=' text-black bg-gray-200 rounded w-5 h-5 grid'>
							<FontAwesomeIcon icon={faEnvelope} className='w-3 m-auto' />
						</div>
						<span>Alidin202@gmail.com</span>
					</span>
					<span className='inline-flex items-start gap-2'>
						<div className=' text-black bg-gray-200 rounded w-5 h-5 grid mt-1'>
							<FontAwesomeIcon icon={faLocationDot} className='w-3 m-auto' />
						</div>
						<span className='flex flex-col'>Lombok Barat, NTB</span>
					</span>
					<span className='inline-flex items-center gap-2'>
						<div className=' text-black bg-gray-200 rounded w-5 h-5 grid'>
							<FontAwesomeIcon
								icon={faBuildingColumns}
								className='w-3 m-auto'
							/>
						</div>
						<span>University of Mataram</span>
					</span>
				</div>
			</div>
			<hr />
			<div className='flex flex-row gap-5'>
				<div>
					<div className='rounded-full bg-white h-32 w-32'></div>
				</div>
				<div>
					Summarise your career here. You can make a PDF version of your resume
					using our free Sketch template here. Donec quam felis, ultricies nec,
					pellentesque eu. Lorem ipsum dolor sit amet, consectetuer adipiscing
					elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
					natoque penatibus et magnis dis parturient montes, nascetur ridiculus
					mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
					sem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut
					libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget
					eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.
				</div>
			</div>

			<div className='flex flex-row'>
				<div className='flex flex-col gap-3 border-r border-gray-700 pr-7'>
					<WorkExperience />
					<Projects />
					<OtherProjects />
				</div>
				<div className='min-w-72 flex flex-col pl-7'>
					<ResumeSidebar />
				</div>
			</div>
		</div>
	);
}

export default Resume;
