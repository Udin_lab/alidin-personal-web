import React from "react";
import PropTypes from "prop-types";
import { Outlet } from "react-router-dom";

PortofolioLayout.propTypes = {};

function PortofolioLayout(props) {
	return <Outlet />;
}

export default PortofolioLayout;
