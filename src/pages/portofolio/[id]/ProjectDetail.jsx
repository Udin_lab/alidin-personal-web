import React from "react";
import { useParams } from "react-router-dom";
import ResultItem from "./component/ResultItem";
import portofolioData from "../../../assets/data/portofolio-data";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLink, faLinkSlash } from "@fortawesome/free-solid-svg-icons";

function ProjectDetail(props) {
	const { id } = useParams();

	const data = portofolioData.find((item) => item.id === id);

	return (
		<div className='flex flex-col gap-10'>
			<div>
				<h3 className='text-center'>{data.title}</h3>
			</div>
			<hr />
			<div className='flex flex-row gap-5 bg-secondary-dark p-10 print:border print:border-gray-200'>
				<div>
					<div className='bg-white  w-60 h-60 rounded overflow-hidden'>
						<img src={data.icon}></img>
					</div>
				</div>
				<div className='flex flex-col gap-2'>
					<h4>Description</h4>
					<p>{data.description}</p>

					<span className='font-bold'>Client : {data.client}</span>
					{data.url && (
						<span className='font-bold'>
							Project URl :{" "}
							<a
								href={"https://" + data.url}
								target='_blank'
								className='hover:text-primary-teal'
							>
								{data.url}
							</a>
						</span>
					)}
				</div>
			</div>

			<div className='flex flex-col gap-3'>
				<h4>My Responsibilites</h4>
				<ul className='list-disc ml-9'>
					{data.responsibily.map((item, index) => (
						<li key={index}>{item}</li>
					))}
				</ul>
			</div>

			<div className='flex flex-col gap-3'>
				<h4>The Challenges</h4>
				<p>{data.challenge}</p>
			</div>

			<div className='flex flex-col gap-3'>
				<h4>Gallery</h4>
				<div className='w-full bg-secondary-dark gap-5 p-10 grid-cols-3 grid print:border print:border-gray-200'>
					{data.gallery.map((item, index) => (
						<div className='rounded shadow overflow-hidden'>
							<img src={item.img} key={index} />
						</div>
					))}
				</div>
			</div>

			<div className='flex flex-col gap-3'>
				<h4>Result</h4>
				<div className='grid grid-cols-3 gap-5'>
					{data.result.map((item, index) => (
						<ResultItem key={index} result={item}></ResultItem>
					))}
				</div>
			</div>
		</div>
	);
}

export default ProjectDetail;
