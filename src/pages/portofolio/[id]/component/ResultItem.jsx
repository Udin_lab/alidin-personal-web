import React from "react";
import PropTypes from "prop-types";

ResultItem.propTypes = {
	result: PropTypes.array,
};

function ResultItem(props) {
	const { result } = props;
	return (
		<div className='bg-secondary-dark flex flex-col gap-2 p-5 print:border print:border-gray-200'>
			<span className='text-primary-teal font-bold '>{result.title}</span>
			<span className='inline-flex items-end'>
				<span className='text-5xl font-bold'> {result.value} </span>
			</span>
		</div>
	);
}

export default ResultItem;
