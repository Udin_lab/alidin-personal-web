import React from "react";
import { useState } from "react";
import portofolioData from "../../assets/data/portofolio-data";
import ProjectItem from "../../components/item/ProjectItem";

function Portofolio(props) {
	const [filter, setFilter] = useState("all");

	return (
		<div className='flex flex-col gap-5 '>
			<div className='text-center'>
				<h3>Portofolio</h3>
			</div>
			<div className='text-center'>
				Get to know me through my projects ! I'm specialized in Fullstack Web
				Development but I also love to learn new things! If you have any
				question regarding my past projects and want to know me better please
				reach out ya!
			</div>
			<div className='flex flex-row justify-center gap-5'>
				<button
					onClick={() => {
						setFilter("all");
					}}
					className={
						"p-2 font-extrabold border-t transition-all duration-300 border-t-primary-dark  " +
						(filter === "all"
							? "border-t-primary-teal text-primary-teal"
							: "text-secondary-light hover:text-primary-light")
					}
				>
					All
				</button>
				<button
					onClick={() => {
						setFilter("Web");
					}}
					className={
						"p-2 font-extrabold border-t transition-all duration-300 border-t-primary-dark hover:text-secondary-teal   " +
						(filter === "Web"
							? "border-t-primary-teal text-primary-teal hover:text-secondary-teal "
							: "text-secondary-light hover:text-primary-light")
					}
				>
					Web app
				</button>
				<button
					onClick={() => {
						setFilter("Fullstack");
					}}
					className={
						"p-2 font-extrabold border-t transition-all duration-300 border-t-primary-dark   " +
						(filter === "Fullstack"
							? "border-t-primary-teal text-primary-teal hover:text-secondary-teal "
							: "text-secondary-light hover:text-primary-light")
					}
				>
					Fullstack
				</button>
				<button
					onClick={() => {
						setFilter("Backend");
					}}
					className={
						"p-2 font-extrabold border-t transition-all duration-300 border-t-primary-dark   " +
						(filter === "Backend"
							? "border-t-primary-teal text-primary-teal hover:text-secondary-teal "
							: "text-secondary-light hover:text-primary-light")
					}
				>
					Backend
				</button>
				<button
					onClick={() => {
						setFilter("Frontend");
					}}
					className={
						"p-2 font-extrabold border-t transition-all duration-300 border-t-primary-dark  " +
						(filter === "Frontend"
							? "border-t-primary-teal text-primary-teal hover:text-secondary-teal "
							: "text-secondary-light hover:text-primary-light ")
					}
				>
					Frontend
				</button>
				<button
					onClick={() => {
						setFilter("Machine Learning");
					}}
					className={
						"p-2 font-extrabold border-t transition-all duration-300 border-t-primary-dark  " +
						(filter === "Machine Learning"
							? "border-t-primary-teal text-primary-teal hover:text-secondary-teal "
							: "text-secondary-light hover:text-primary-light ")
					}
				>
					Machine Learning
				</button>
			</div>
			<div className='grid lg:grid-cols-2 grid-cols-1 gap-5'>
				{/* items goes here */}
				{portofolioData
					.filter((value) => {
						return filter === "all" || value.tag.includes(filter);
					})
					.map((item) => (
						<ProjectItem project={item} key={item.id} />
					))}
			</div>
		</div>
	);
}

export default Portofolio;
