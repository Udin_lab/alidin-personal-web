import {
	faBootstrap,
	faJava,
	faJs,
	faLaravel,
	faNodeJs,
	faPhp,
	faPython,
	faReact,
} from "@fortawesome/free-brands-svg-icons";

export default [
	{
		id: "javascript",
		title: "JavaScript, React, ExpressJs",
		icons: [faJs, faReact, faNodeJs],
		description: [
			"JavaScript is a high-level and dynamic programming language. JavaScript is popular on the internet and works in most popular web browsers.",
			"React is a free and open source front-end JavaScript library for building user interfaces based on UI components",
			"Expressjs is a back-end web application Framework for Node.js, released as free and open source software under the MIT License",
		],
	},

	{
		id: "python",
		title: "Python, Tensorflow",
		icons: [faPython],
		description: [
			"Python is an interpreted, high-level general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with significant use of whitespace.",
			"TensorFlow is a free and open source software library for machine learning. This library can be used for a variety of purposes, but has a particular focus on training and inference of deep neural networks.",
		],
	},

	{
		id: "php",
		title: "PHP, Laravel",
		icons: [faPhp, faLaravel],
		description: [
			"Hypertext Preprocessor or PHP, is a scripting language with general functionality mainly used for web development.",
			"Laravel is an open source PHP-based web application framework, using the Model-View-Controller concept. Laravel is under the MIT license, using GitHub as a place to share code.",
		],
	},

	{
		id: "css-fw",
		title: "Tailwind-CSS, Bootstrap",
		icons: [faBootstrap],
		description: [
			"Tailwind provides a tool for extracting component classes from iterative utility patterns, which makes it easy to update multiple instances in a component from one place.",
			"Bootstrap is a free and open source CSS framework for designing websites and web applications. This framework contains HTML and CSS based design templates for typography, forms, buttons, navigation, and other interface components.",
		],
	},

	{
		id: "dart",
		title: "Dart, Flutter",
		icons: ["dart", "flutter"],
		description: [
			"Dart is a programming language designed for client development, such as for web and mobile applications. It was developed by Google and can also be used to build server and desktop applications.",
			"Flutter is an open source mobile application framework created by Google. Flutter is used in application development for Android, iOS, Windows, Linux, MacOS operating systems, as well as being the main method for creating Google Fuchsia applications.",
		],
	},

	{
		id: "db",
		title: "SQL, MongoDB",
		icons: ["mysql", "mongodb"],
		description: [
			"SQL is a language used to access data in relational databases. It is the de facto standard language used in relational database management.",
			'MongoDB is a cross-platform document-oriented database system. Classified as a "NoSQL" database, MongoDB eschews traditional table-based relational database structures in favor of JSON like documents with dynamic schemas.',
		],
	},

	{
		id: "java",
		title: "Java",
		icons: [faJava],
		description: [
			"Java is a programming language that can be run on various computers including mobile phones. This language was originally created by James Gosling while still at Sun Microsystems, which is currently part of Oracle and was released in 1995.",
		],
	},
];
