import toeflUpIcon from "./gallery/toefl-up/icon.png";
import othetoeIcon from "./gallery/othetoe/icon.png";
import maskDetectIcon from "./gallery/mask-detection/icon.jpg";
import butterflyIcon from "./gallery/butterfly-classification/icon.jpg";
import mdmIcon from "./gallery/mdm/icon.png";
import segrakatonIcon from "./gallery/segarakaton/icon.png";
import covidiaIcon from "./gallery/covidia/icon.jpeg";
import othetoe from "./gallery/othetoe";
import segarakaton from "./gallery/segarakaton";
import butterflyClassification from "./gallery/butterfly-classification";
import toeflUp from "./gallery/toefl-up";
import covidia from "./gallery/covidia";
import mdm from "./gallery/mdm";
import maskDetection from "./gallery/mask-detection";

export default [
	{
		id: "covidia",
		icon: covidiaIcon,
		title: "COVIDIA : Covid Diagnose Aplication",
		tag: ["Machine Learning", "Backend"],
		client: "PSTI Unram",
		tech: ["Python", "Flask", "Tensorflow"],
		url: "",
		description:
			"Successfully developed a new machine learning model that can \
    accurately classify Covid-19, normal, and pneumonia lungs based on \
    X-ray gallery with a 98% accuracy rate. The model is currently hosted \
    using a Flask Rest API so another developer can access it to \
    classify a large number of lung X-ray gallery. This project is led by \
    I Gede Pasek Suta, the Head of Informatics Engineering Department. \
    Mataram University.",

		responsibily: [
			"Create an efficient Machine Learning Model to predict the COVID-19 based on the x-ray image",
			"Create a reliable, secure and fast REST API to serve the Machine Learning Model",
		],

		challenge:
			"This project is a collaboration with Professor I Gede Suta Wijaya, the Head of Informatics Engineering Department of Universitas Mataram.  The main challenge in this project is how to create a machine learning model that have a high accuracy, a speed of execution, a security, a reliability, with the small sized model.",
		result: [
			{
				id: "covidia-1",
				title: "Accuracy Rate",
				value: "98%",
			},
			{
				id: "covidia-2",
				title: "Income",
				value: "Rp. 6.000.000",
			},
			{
				id: "covidia-3",
				title: "Copyright",
				value: "1",
			},
		],
		gallery: covidia,
	},
	{
		id: "toefl-up",
		icon: toeflUpIcon,
		title: "TOEFL-UP : TOEFL Test Prediction App",
		tag: [
			"Fullstack",
			"Frontend",
			"Backend",
			"ReactJs",
			"Laravel",
			"RestApi",
			"Web",
		],
		tech: ["ReactJS", "Laravel", "Rest API"],
		client: "Rumah Bahasa LPPNTB",
		url: "toefl-up.lppntb.com",
		responsibily: [
			"Create a fullstack application using ReactJs, Laravel and RestApi",
			"Create a secure, fast, and reliable REST API to provide the entire data from the Backend to the Frontend",
			"Create a responsive front end using ReactJs",
		],
		description:
			"This application is a part of project for campus intership, made\
    specifically to predict TOEFL scores of participants that enrolled\
    into Rumah Bahasa program LPPNTB, this application has been\
    developed up to version 3.0, and is still actively used to conduct\
    regular TOEFL exams. Developed by using ReactJs and Laravel.",
		challenge:
		"I managed to release the version 1.0 within 3 months of development although this project requires the ability to process complex databases, this project structure is also very complicated, and relatively have too many features to be developed only  by 1 developer. Considering this is a full-stack web development within my experience of learning React for about 1 month, and learning Laravel rest api for about 2 months, this is a very sucessful project and now I've launch the version 3.0 that have been use regullary  to predict TOEFL EXAM at Rumah Bahasa LPPNTB.",
		result: [
			{
				id: "toefl-up-1",
				title: "Total Line Of Code",
				value: "20.000+",
			},
			{
				id: "covidia-2",
				title: "Income",
				value: "Rp. 3.500.000",
			},
		],
		gallery: toeflUp,
	},
	{
		id: "esign-eform-segarakaton",
		icon: segrakatonIcon,
		title: "Segara Katon E-Sign & E-Form App",
		tag: [
			"Fullstack",
			"Frontend",
			"Backend",
			"ReactJs",
			"Laravel",
			"RestApi",
			"Web",
		],
		tech: ["ReactJS", "Laravel", "Rest API"],
		client: "Pemerintah Desa Segara Katon",
		url: "gitlab.com/d4657",
		responsibily: [
			"Create a fullstack application using ReactJs, Laravel and RestApi",
			"Create a secure, fast, and reliable REST API to provide the entire data from the backend to the front end",
			"Create a responsive Frontend using ReactJs",
		],
		description:
			"This application manange data collection of incoming and outgoing \
    letters, as well as online signature service for Segara village's \
    office, the frontend of this application are developed using ReactJs \
    while the backend are developend using Laravel Rest API. I developed \
    this application during my comunity service course 2022.",
		challenge:
			"The main challenge in this application is that it has to be developed within 1 month, moreover I have to share my time with other work programs, this project also requires image processing such as generating a QR-Code and then combining it with the user's signature",
		result: [
			{ id: "segarakaton-1", title: "Total Line Of Code", value: "5000+" },
			{ id: "segarakaton-2", title: "Grade", value: "A" },
		],
		gallery: segarakaton,
	},
	{
		id: "othetoe",
		icon: othetoeIcon,
		title: "Othetoe Game",
		tag: ["Fullstack", "Game"],
		tech: ["Java", "Swing"],
		client: "-",
		url: "gitlab.com/Udin_lab/tubes_pbo",
		responsibily: [
			"Create a game application using Java and Swing Library",
			"Create the entire game assets",
		],
		description:
			"Othetoe is a game developed for an object-oriented programming course \
      project, this game is a combination of tictactoe and othelo games where \
      players will get points if they manage to align the same symbols according \
      to their roles. the player wins when it reaches the predetermined point",
		challenge:
			"To get full marks, the application must be delevelled for less than 3 weeks, then there are several conditions that must be met by students given by lecturers and practicum assistants, and the most difficult thing is how to build this application without using a framework at all",
		result: [
			{ id: "othetoe-1", title: "Total Line Of Code", value: "2700+" },
			{ id: "othetoe-2", title: "Grade", value: "A" },
		],
		gallery: othetoe,
	},
	{
		id: "mdm",
		icon: mdmIcon,
		title: "Minimal Download Manager",
		tag: ["Fullstack", "Utility"],
		tech: ["Java", "Swing"],
		url: "gitlab.com/Udin_lab/minimaldownloadmanager",
		client: "-",
		responsibily: ["Create a download utility app using Java and SwingLibrary"],
		description:
			"This app is a game developed for an intenet programming course project, this application can be used in multiple desktop platform as long as the java run time environtment is installed . ",
		challenge:
			"The application must be delevelled for less than 2 weeks, then there are several conditions that must be met based on the mandatory requirements given by the course lecturer, and the most difficult thing is how to build this application without using a framework and having to use limited libraries that are only allowed by the lecturer",
		result: [
			{ id: "mdm-1", title: "Total Line Of Code", value: "2000+" },
			{ id: "mdm-2", title: "Grade", value: "A" },
		],
		gallery: mdm,
	},
	{
		id: "mask-detection",
		icon: maskDetectIcon,
		title: "Mask Detection",
		tag: ["Machine Learning"],
		tech: ["Python", "Tensorflow"],
		client: "-",
		url: "gitlab.com/Udin_lab/tubes-app-ai",
		description:
			"This is a project for the AI Applications course. The machine learning model has an accuracy of 91%. When testing the model, the model has an FPS of more than 30 which can be considered to have very good performance, and the total number of datasets is around 1000.",
		responsibily: [
			"Mining the datasets from multiple source",
			"Preprocess the datasets",
			"Create a machine learning model to detect whether a person is wearing a mask or not",
		],
		challenge:
			"The machine learning model that is made must have an accuracy above 90%, and must be developed in a relatively short period of time, which is only for 2 weeks. And with laptop specifications that are not too high and a large number of datasets, the execution process will take a very long time to complete.",
		result: [
			{ id: "mask-1", title: "Accuracy", value: "91%" },
			{ id: "mask-2", title: "Grade", value: "A" },
		],
		gallery: maskDetection,
	},
	{
		id: "butterfly-classification",
		icon: butterflyIcon,
		title: "Butterfly Classification",
		tag: ["Machine Learning"],
		tech: ["Python", "Tensorflow"],
		client: "-",
		url: "colab.research.google.com/drive/146IlRD5qa-bztfSmdxNQih3yyH_crABr",
		responsibily: [
			"Mining the datasets from multiple source",
			"Preprocess the datasets",
			"Create a machine learning model that can successfully cassify any given butterfly species",
		],
		description:
			"This is a project for the Artificial Neural Network course. The machine learning model created is able to identify various types of butterflies, the model has an average accuracy of 96%",
		challenge:
			"Machine learning models must be built in several scenarios, then explain the advantages and disadvantages of these scenarios and compare each architecture based on performance, accuracy, and computational efficiency",
		result: [
			{ id: "mask-1", title: "Accuracy", value: "98%" },
			{ id: "mask-2", title: "Grade", value: "A" },
		],
		gallery: butterflyClassification,
	},
];
