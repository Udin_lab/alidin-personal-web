import cover from "./1.jpg";
import lampiran from "./2.jpg";
import repository from "./3.png";
import foto1 from "./4.jpeg";
import foto2 from "./5.jpeg";

export default [
	{
		title: "Cover",
		img: cover,
	},

	{
		title: "Lampiran",
		img: lampiran,
	},
	{
		title: "Repository",
		img: repository,
	},
	{
		title: "Foto 1",
		img: foto1,
	},
	{
		title: "Foto 2",
		img: foto2,
	},
];
