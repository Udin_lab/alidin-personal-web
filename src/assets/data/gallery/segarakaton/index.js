import homeScreen from "./0.png";
import validation from "./1.png";
import profile from "./2.png";
import users from "./3.png";
import form from "./4.png";
import esign from "./5.png";
import signed from "./6.png";
import discussion from "./7.jpg";

export default [
	{
		title: "Home Screen",
		img: homeScreen,
	},
	{
		title: "Validation",
		img: validation,
	},
	{
		title: "Profile",
		img: profile,
	},
	{
		title: "Users",
		img: users,
	},
	{
		title: "Form",
		img: form,
	},
	{
		title: "E-Sign",
		img: esign,
	},
	{
		title: "Signed Form",
		img: signed,
	},

	{
		title: "Discussion",
		img: discussion,
	}
];
