import summary from "./1.png";
import preview from "./2.png";
import performance from "./3.png";
import confusionMatrix from "./4.png";

export default [
	{
		title: "Summary",
		img: summary,
	},
	{
		title: "Preview",
		img: preview,
	},

	{
		title: "Performance",
		img: performance,
	},

	{
		title: "Confusion Matrix",
		img: confusionMatrix,
	},
];
