import downloadList from "./1.png";
import newDownload from "./2.png";
import downloadProgress from "./3.png";

export default [
	{
		title: "Download List",
		img: downloadList,
	},
	{
		title: "New Download",
		img: newDownload,
	},
	{
		title: "Download Progress",
		img: downloadProgress,
	},
];
