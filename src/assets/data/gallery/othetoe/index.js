import homeScreen from "./1.png";
import leaderboard from "./2.png";
import history from "./3.png";
import gamesetup from "./4.png";
import gameplay from "./5.png";

export default [
	{
		title: "Home Screen",
		img: homeScreen,
	},
	{
		title: "Leaderboard",
		img: leaderboard,
	},
	{
		title: "History",
		img: history,
	},
	{
		title: "Game Setup",
		img: gamesetup,
	},
	{
		title: "Gameplay",
		img: gameplay,
	},
];
