import result from "./1.png";
import trainingResult from "./1.png";
import modelLoss from "./1.png";
import modelAccuracy from "./1.png";

export default [
	{
		title: "result",
		img: result,
	},

	{
		title: "Training Result",
		img: trainingResult,
	},
	{
		title: "Model Loss",
		img: modelLoss,
	},
	{
		title: "Model Accuracy",
		img: modelAccuracy,
	},
];
