import homeScreen from "./1.png";
import dashboard from "./2.png";
import toefl from "./3.png";
import Schedules from "./4.png";
import Instruction from "./5.png";
import users from "./6.png";
import profile from "./7.png";
import user from "./8.png";
import examScreen from "./9.png";

export default [
	{
		title: "Home Screen",
		img: homeScreen,
	},
	{
		title: "Dashboard",
		img: dashboard,
	},
	{
		title: "Toefl",
		img: toefl,
	},
	{
		title: "Schedules",
		img: Schedules,
	},
	{
		title: "Instruction",
		img: Instruction,
	},
	{
		title: "Users",
		img: users,
	},
	{
		title: "Profile",
		img: profile,
	},
	{
		title: "User",
		img: user,
	},
	{
		title: "Exam Screen",
		img: examScreen,
	},
];
