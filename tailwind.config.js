module.exports = {
	content: ['./src/**/*.{js,jsx,ts,tsx}'],
	mode: 'jit',
	theme: {
		extend: {
			textColor: {
				skin: {
					primary: '#F2F3F5',
					secondary: '#B9BABC',
				},
			},

			spacing: {
				13: '3.25rem',
				15: '3.75rem',
				17: '4.25rem',
				18: '4.5rem',
				30: '7.5rem',
				42: '10.5rem',
				76: '19rem',
				82: '20.5rem',
				84: '21rem',
				128: '32rem',
				144: '36rem',
				180: '45rem',
				200: '50rem',
				400: '100rem',
			},

			minWidth: {
				'1/2': '50%',
				'4/5': '80%',
				72: '18rem',
				170: '42.5rem',
				180: '45.5rem',
				200: '50rem',
			},

			minHeight: {
				200: '50rem',
			},

			maxHeight: {
				80: '20rem',
			},

			rotate: {
				135: '135deg',
			},

			colors: {
				primary: {
					light: '#F2F3F5',
					dark: '#131922',
					semi: '#2E425C',
					teal: '#54B788',
				},
				secondary: {
					dark: '#1E2B3B',
					light: '#B9BABC',
					semi: '#2d3f54',
          'semi-alt' : '#141E28',
					teal: '#3f8a67',
				},
			},
		},
	},
	plugins: [require('@tailwindcss/typography')],
};
